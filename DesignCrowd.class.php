<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class DesignCrowd
 *
 * @package Parsers
 */

class DesignCrowd extends Parser{

    protected $ordersPerPage = 40;

    public $baseURL = "http://jobs.designcrowd.com";

    protected $categories = [
        "Main" => "http://jobs.designcrowd.com/graphic-design-jobs.aspx?design=web-design",
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => '/html/body/div[1]/div[3]/form/div[4]/div/div/div/div[1]/div[2]/div/div/table/tbody/tr[{{ITERATOR}}]/td[2]/a/@href',
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[1]/div[3]/form/div[4]/div/div/div/div[1]/div[2]/div/div/table/tbody/tr[{{ITERATOR}}]/td[2]/a/@title",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => '//*[@id="cphBaseMasterPageBody_mainContent_briefSummary_lblTask"]',
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/html/body/div[1]/div[3]/form/div[4]/div/div/div/div[1]/div[2]/div/div/table/tbody/tr[{{ITERATOR}}]/td[6]/strong/span",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 1
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime(){
        return date('Y-m-d H:i:s', strtotime("Today"));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice($value){
        return (float) preg_replace("/[^0-9.]/","", $value);
    }

    public function filter_orderTitle($value){
        return $value == "This project has been marked at private by the customer. To view details of this brief, users must log in to the system." ? false : true;
    }


}

$test = new DesignCrowd();
$test->startParsing();