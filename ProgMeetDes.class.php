<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class ProgMeetDes
 *
 * @package Parsers
 */

class ProgMeetDes extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "http://www.programmermeetdesigner.com/";

    protected $categories =     [
        "Main" => "http://www.programmermeetdesigner.com"
    ];


    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[2]/div[2]/ol/li[{{ITERATOR}}]/h3/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[2]/div[2]/ol/li[{{ITERATOR}}]/table/tr[4]/td",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[2]/div[2]/ol/li[{{ITERATOR}}]/h3/a",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderSkills" =>    [
            "xPath" => "/html/body/div[2]/div[2]/ol/li[{{ITERATOR}}]/table/tr[1]/td/p",
            "count" => "single",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[2]/div[2]/ul/li[2]",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 1
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        $date = explode("on", $value);
        return date('Y-m-d H:i:s', strtotime($date[1 ]));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice(){
        return "0";
    }

    public function iteratorValue($step){
        return 2*$step;
    }

    public function decorator_orderTitle($value){
        return trim($value);
    }

    public function decorator_orderDescription($value){
        return trim(substr(trim(strip_tags($value)), 17));
    }

}

$test = new ProgMeetDes();
$test->startParsing();