<?php

require_once('OrderFilter.trait.php');
require_once('SkillsDetector.trait.php');
require_once('alchemyapi.php');

/**
 * Class Order
 *
 * @package ParserCore
 */

Class Order
{
    use Cli, SkillsDetector;

    private $status = true;
    private $categoryDOM;
    private $orderDOM;
    private $pathes;
    private $orderIterator;
    private $parser;
    //public  $alchemy;

    public static $orderValues = [] ;


    /**
     * Creating new object Order
     *
     * @param   object $categoryDOM DOM object of parent category page where link to order is placed
     * @param   string $orderIterator Counting number of order on category page
     * @param   object $parser Self object of current parser
     */

    public function __construct($categoryDOM, $orderIterator, $parser)
    {
        $this->categoryDOM = $categoryDOM;
        $this->pathes = $parser->parsePathes;
        $this->orderIterator = $orderIterator;
        $this->parser = $parser;
        //$this->alchemy = new AlchemyAPI();

        if(!$this->orderDOM){
            if($this->parser->baseURL){
                $this->orderDOM = $this->parser->getPageDOMxPath($this->parser->baseURL.$this->getOrderValue("orderURL"));
            }else{
                $this->orderDOM = $this->parser->getPageDOMxPath($this->getOrderValue("orderURL"));
            }
        }

    }

    /**
     * Preparing order URL address
     *
     * @param   string $path xPath of order URL on category page
     * @param   integer $iterator Index number of order on category page
     * @return  mixed               Prepared order path
     */

    public function preparePath($path, $iterator)
    {
        return str_replace("{{ITERATOR}}", $iterator, $path);
    }

    /**
     * Parsing order value by key and passing it into decoration function which declared in @see parser
     *
     * @param   string $key Name of parsing value
     * @return  mixed           Parsed value
     */

    public function getOrderValue($key)
    {
        $path = $this->preparePath($this->pathes[$key]['xPath'], $this->orderIterator);

        switch ($this->pathes[$key]["page"]) {
            case "category":
                $path = $this->categoryDOM->query($path);
                break;

            case "order":
                $path = $this->orderDOM->query($path);
                break;

            default:
                $this->showMessage("Incorrect path type \"" . $this->parsePathes[$key]["count"] . "\" in " . $key . " path!", "error");
                break;
        }

        switch ($this->pathes[$key]["count"]) {
            case "single":
                foreach ($path as $entry) {
                    $result = $entry->nodeValue;
                }

                break;

            case "multiple":
                $result = [];
                foreach ($path as $entry) {
                    $result[] = $entry->firstChild->nodeValue;
                }

                $result = json_encode($result);

                break;

            default:
                $this->showMessage("Incorrect counting \"" . $this->parsePathes[$key]["count"] . "\" in " . $key . " path!", "error");
                break;

        }

        if ($this->pathes[$key]["decorator"]) {
            if (method_exists($this->parser, "decorator_".$key)) {
                $result = $this->parser->{"decorator_".$key}($result);
            } else {
                $this->showMessage("Parameter ".$key." needs decorator!", "error");
            }
        }

           //echo $key." - ".$result.PHP_EOL;


        return $result;
    }

    /**
     * Processing order and checking its values.
     * If order has not passed filtering its status changes on FALSE.
     * Otherwise, calls method @see addOrder
     */

    public function processOrder()
    {
        foreach(array_keys($this->pathes) as $orderValue){
            self::$orderValues[$orderValue] = $this->getOrderValue($orderValue);
            if(method_exists($this->parser, "filter_".$orderValue)){
                if(!$this->parser->{"filter_".$orderValue}(self::$orderValues[$orderValue])){
                    $this->status = false;
                }
            }
        }

        if($this->status){
            $data = [];

            $skills = json_encode($this->processSkills(self::$orderValues['orderTitle'], self::$orderValues['orderDescription'], self::$orderValues['orderSkills']));

            $data['title'] = self::$orderValues['orderTitle'];
            $data['description'] = trim(substr(self::$orderValues['orderDescription'], 0, 199));
            $data['url'] = $this->parser->baseURL . self::$orderValues['orderURL'];
            $data['price'] = self::$orderValues['orderPrice'];
            $data['payment_type'] = self::$orderValues['orderType'];
            $data['skills'] = $skills;
            $data['date'] = self::$orderValues['postedTime'];

            //var_dump($data);

            $this->addOrder($data);

        }


    }

    /**
     * @param $data
     */

    private function addOrder($data){
        $stmt = $this->parser->DBConnection->prepare("INSERT INTO offers (id, title, description, url, price, payment_type, skills, `date`) VALUES ('',?,?,?,?,?,?,?)");
        $stmt->bind_param("sssssss", $data['title'], $data['description'], $data['url'], $data['price'], $data['payment_type'], $data['skills'], $data['date']);
        $stmt->execute();

        $this->showMessage("Added: ".$data['title'], "info");


    }





}