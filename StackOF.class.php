<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class StackOF
 *
 * @package Parsers
 */

class StackOF extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "http://careers.stackoverflow.com";

    protected $categories =     [
        "Main" => "http://careers.stackoverflow.com/jobs?allowsremote=true&pg={{ITERATOR}}"
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[2]/div/div[1]/div[1]/div[2]/div[1]/div[{{ITERATOR}}]/h3/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[2]/div/div[1]/div[1]/div[2]/div[1]/div[{{ITERATOR}}]/p[1]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[2]/div/div[1]/div[1]/div[2]/div[1]/div[{{ITERATOR}}]/h3/a",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/html/body/div[2]/div/div[1]/div[1]/div[2]/div[1]/div[{{ITERATOR}}]/div/p/a",
            "count" => "multiple",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[2]/div/div/div[2]/div/div[2]",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 2
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime(trim(preg_replace("/[^ \w]+/", "", $value))));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice($value){
        return "0";
    }

    public function decorator_orderDescription($value){
        return trim(trim($value), "Job Description");
    }



}

$test = new StackOF();
$test->startParsing();