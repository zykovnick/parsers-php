<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class jobWordpress
 *
 * @package Parsers
 */

class jobWordpress extends Parser{

    protected $ordersPerPage = 20;

    public $baseURL = "";

    protected $categories =     [
        "General" => "http://jobs.wordpress.net/job_category/general/",
        "Migration" => "http://jobs.wordpress.net/job_category/migration/",
        "Performance" => "http://jobs.wordpress.net/job_category/performance/",
        "Plugin Development" => "http://jobs.wordpress.net/job_category/plugin-development/",
        "Theme Customization" => "http://jobs.wordpress.net/job_category/theme-customization/",
        "Writing" => "http://jobs.wordpress.net/job_category/writing/",
        "Design" => "http://jobs.wordpress.net/job_category/design/",
        "Development" => "http://jobs.wordpress.net/job_category/development/"
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div/div[2]/div[2]/main/div/div[{{ITERATOR}}]/div[2]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div/div[2]/div[2]/main/div/div[{{ITERATOR}}]/div[1]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div/div[2]/div[2]/main/div/div[{{ITERATOR}}]/div[2]/a",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => '/html/body/div/div[2]/div[2]/main/div/article/div[2]/div',
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ],
        "orderType" =>      [
            "xPath" => "/html/body/div/div[2]/div[2]/main/div/div[{{ITERATOR}}]/div[3]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 4
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime($value));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice(){
        return "0";
    }

    public function decorator_orderDescription($value){
        return $value;
    }

    public function filter_orderType($value){
        return $value != "Full Time" ? true : false;
    }


}

$test = new jobWordpress();
$test->startParsing();