<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class SmashMag
 *
 * @package Parsers
 */

class SmashMag extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "";

    protected $categories =     [
        "Main" => "http://jobs.smashingmagazine.com/freelance"
    ];


    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "//*/ul[@class='entry-list compact']/li[{{ITERATOR}}]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "//*/li[@class='date']",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "//*/ul[@class='entry-list compact']/li[{{ITERATOR}}]/a/article/h2",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => "//*/article['post job-entry']/p[1]",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime($value));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice(){
        return "0";
    }

    public function decorator_orderDescription($value){
        return $value;
    }

}

$test = new SmashMag();
$test->startParsing();