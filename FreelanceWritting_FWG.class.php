<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class FreelanceWritting_FWG
 *
 * @package Parsers
 */

class FreelanceWritting_FWG extends Parser{

    protected $ordersPerPage = 19;

    public $baseURL = "";

    protected $categories =     [
        "Freelance stories" => "http://www.freelancewriting.com/freelancejobs/freelance-writing-gigs.php",
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[5]/div/div/div[1]/div[{{ITERATOR}}]/div/div[4]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[5]/div/div/div[1]/div[{{ITERATOR}}]/div/div[1]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[5]/div/div/div[1]/div[{{ITERATOR}}]/div/h2",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[5]/div/div/div[1]/div[9]/div/div[3]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 8
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        $date = substr($value,7);
        return date('Y-m-d H:i:s', strtotime($date));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice(){
        return "0";
    }


}

$test = new FreelanceWritting_FWG();
$test->startParsing();