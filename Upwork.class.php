<?php

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class Upwork
 *
 * @package Parsers
 */

class Upwork extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "https://www.upwork.com";

    protected $categories =     [
                                "Web, mobile and software development" => "https://www.upwork.com/o/jobs/browse/c/web-mobile-software-dev/?sort=create_time+desc&page={{ITERATOR}}",
                                "Design and creative" => "https://www.upwork.com/o/jobs/browse/c/design-creative/?sort=create_time+desc&page={{ITERATOR}}",
                                "Writing" => "https://www.upwork.com/o/jobs/browse/c/writing/?sort=create_time+desc&page={{ITERATOR}}",
                                "Sales and marketing" => "https://www.upwork.com/o/jobs/browse/c/sales-marketing/?sort=create_time+desc&page={{ITERATOR}}"
                                ];

    public $parsePathes = [
                                "orderURL" =>       [
                                                        "xPath" => "//*/div/article[{{ITERATOR}}]/header/h2/a/@href",
                                                        "count" => "single",
                                                        "page"  => "category"
                                                    ],
                                "authorRate" =>     [
                                                        "xPath" => "//html/body/div[1]/div[1]/div[2]/div[2]/div/div/div/span[2]/span[1]",
                                                        "count" => "single",
                                                        "page"  => "order",
                                                        "decorator" => true
                                                    ],
                                "postedTime" =>     [
                                                        "xPath" => "//*/div/article[{{ITERATOR}}]/div[2]/span/time/@datetime",
                                                        "count" => "single",
                                                        "page"  => "category",
                                                        "decorator" => true
                                                    ],
                                "orderTitle" =>     [
                                                        "xPath" => "//html/body/div/div[1]/div[1]/div/h1",
                                                        "count" => "single",
                                                        "page"  => "order"
                                                    ],
                                "orderSkills" =>    [
                                                        "xPath" => "//html/body/div/div[1]/div[2]/div[1]/a",
                                                        "count" => "multiple",
                                                        "page"  => "order",
                                                        "decorator" => true
                                                    ],
                                "orderDescription" => [
                                                        "xPath" => "/html/body/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/p[1]",
                                                        "count" => "single",
                                                        "page"  => "order"
                                                    ],
                                "orderType" =>      [
                                                        "xPath" => "//html/body/div/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/p/strong",
                                                        "count" => "single",
                                                        "page"  => "order",
                                                        "decorator" => true
                                                    ],
                                "orderPrice" =>     [
                                                        "xPath" => "//html/body/div/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/p/strong",
                                                        "count" => "single",
                                                        "page"  => "order",
                                                        "decorator" => true
                                                    ]
                                ];


    public $configUniq = [
                        "orderMaxAge" => 86400
                        ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime($value));
    }

    public function decorator_orderType($value){
        switch($value){
            case "Fixed Price":
                $type = "fixed";
                break;

            case "Hourly Job":
                $type = "hourly";
                break;

            default:
                break;
        }

        return $type;
    }

    public function decorator_orderPrice($value){
        return (float) preg_replace("/[^0-9.]/","", $value);
    }

    public function decorator_orderSkills($value){
        return $value;
    }

    public function decorator_authorRate($value){
        return (float) $value;
    }

    public function filter_authorRate($value){
        return $value >= 3 ? true : false;
    }

    public function filter_orderType($value){
        if (Order::$orderValues["orderPrice"] != 0 ){
            switch ($value){
                case "fixed":
                    return Order::$orderValues["orderPrice"] >= $this->config["minimalFixedPrice"] ? true : false;
                    break;

                case "hourly":
                    return Order::$orderValues["orderPrice"] >= $this->config["minimalHourlyPrice"] ? true : false;
                    break;

                default:
                    break;
            }
        }else{
            return true;
        }
    }



}

$test = new Upwork();
$test->startParsing();