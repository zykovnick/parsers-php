<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class getaCoder
 *
 * @package Parsers
 */

class getaCoder extends Parser{

    protected $ordersPerPage = 20;

    public $baseURL = "";

    protected $categories =     [
        "Main" => "http://www.getacoder.com/requests/web%20design%20development_42.htm"
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "//*/table[{{ITERATOR}}]/tr/td[2]/table/tr/td/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "//*/table[{{ITERATOR}}]/tr/td[7]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "//*/table[{{ITERATOR}}]/tr/td[2]/table/tr/td/a",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "//*/table[{{ITERATOR}}]/tr/td[6]/a",
            "count" => "multiple",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => '//*[@id="descr1"]',
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ],
        "orderType" =>      [
            "xPath" => "//*/table[{{ITERATOR}}]/tr/td[3]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "//*/table[{{ITERATOR}}]/tr/td[4]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 2
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime($value));
    }

    public function decorator_orderType($value){
        return trim($value) == "Project" ? "fixed" : "hourly";
    }

    public function decorator_orderPrice($value){
        return (float) preg_replace("/[^0-9.]/","", $value);
    }

    public function decorator_orderDescription($value){
        $value = substr(trim($value), 11);
        return $value;
    }

    public function iteratorValue($step){
        return 2*$step-1;
    }


}

$test = new getaCoder();
$test->startParsing();