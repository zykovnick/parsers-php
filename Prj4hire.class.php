<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class Prj4hire
 *
 * @package Parsers
 */

class Prj4hire extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "";

    protected $categories =     [
        "Main" => "http://www.project4hire.com/search.php?mode=service&sort=02&page={{ITERATOR}}&pp=10"
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "//*/form/div[1]/div[{{ITERATOR}}]/div[1]/h3/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "//*/form/div[1]/div[{{ITERATOR}}]/div[1]/h2[1]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "//*/form/div[1]/div[{{ITERATOR}}]/div[1]/h3/a",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "//*/form/div[1]/div[{{ITERATOR}}]/div[1]/h2[3]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => "//*/div[4]/div/div",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50
    ];


    public function __construct(){
        parent::__construct();
    }

    public function iteratorValue($step){
        return 2*$step-1;
    }

    public function decorator_postedTime($value){

        $dateInfo = explode("|", $value);

        $date = substr($dateInfo[1], 12);
        $date = substr($date, 0, -4);


        return date('Y-m-d H:i:s', strtotime($date));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice($value){
        return "0";
    }

    public function decorator_orderDescription($value){
        return $value;
    }



}

$test = new Prj4hire();
$test->startParsing();