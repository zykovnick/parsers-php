<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class Joomlancers
 *
 * @package Parsers
 */

class Joomlancers extends Parser{

    protected $ordersPerPage = 20;

    public $baseURL = "http://www.joomlancers.com";

    protected $categories =     [
        "Main" => "http://www.joomlancers.com/index.php?tmpl=component&option=com_jl_project&controller=listproject&task=search&ajax=1&keyword=&status=open&project_type=0&catid=-1&closing_range=0&listed_range=0&bids_range=0&budgetrange=0&sort=start&order=desc&limit=30&projectTabs=0"
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/table/tr[{{ITERATOR}}]/td[1]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/table/tr[{{ITERATOR}}]/td[3]/div",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/table/tr[{{ITERATOR}}]/td[1]/a",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => '/html/body/div[3]/div/div/div/div/div[1]/div[1]/div',
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/html/body/table/tr[{{ITERATOR}}]/td[2]/span",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 10,
        "orderIterator" => 1
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        $date = explode(" ", $value);
        return date('Y-m-d H:i:s', strtotime($date[0]));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice($value){
        $types = ["N/A", "Any budget", "Confidential", "Not sure"];

        if (in_array($value, $types)){
            return "0";
        }else{
            $price = explode(" - ", $value);
            return (float) preg_replace("/[^0-9.]/","", $price[1]);
        }

    }

    public function decorator_orderDescription($value){
        return $value;
    }

    public function iteratorValue($step){
        return 2*$step;
    }

}

$test = new Joomlancers();
$test->startParsing();