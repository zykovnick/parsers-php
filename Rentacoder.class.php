<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class RentaCoder
 *
 * @package Parsers
 */

class RentaCoder extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "";

    protected $categories =     [
        "Main" => "http://www.rent-acoder.com/allProjects.php?pag={{ITERATOR}}"
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[1]/div/div[6]/div[2]/div[1]/div[2]/div[{{ITERATOR}}]/div[1]/h3/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[1]/div/div[6]/div[2]/div[1]/div[2]/div[{{ITERATOR}}]/div[1]/div[2]/p[1]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[1]/div/div[6]/div[2]/div[1]/div[2]/div[{{ITERATOR}}]/div[1]/h3/a",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[1]/div/div[6]/div[2]/div[1]/div[2]/div/p[5]",
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/html/body/div[1]/div/div[6]/div[2]/div[1]/div[2]/div[{{ITERATOR}}]/div[2]/div[1]/a",
            "count" => "single",
            "page"  => "category"
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime(str_replace('/', '-', preg_replace("/[^0-9,.\/]/", "", trim($value)))));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function iteratorValue($step){
        return 2*$step-1;
    }


}

$test = new RentaCoder();
$test->startParsing();