<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class WWRem1
 *
 * @package Parsers
 */

class WWRem1 extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "https://weworkremotely.com";

    protected $categories =     [
        "Main" => "https://weworkremotely.com/"

    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[2]/div/section[2]/article/ul/li[{{ITERATOR}}]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[2]/div/section[2]/article/ul/li[{{ITERATOR}}]/a/span[3]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[2]/div/section[2]/article/ul/li[{{ITERATOR}}]/a/span[2]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "order"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[2]/div[3]/section/div[1]",
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 2
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime(trim($value)));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderDescription($value){
        return strip_tags($value);
    }

    public function decorator_orderPrice(){
        return "0";
    }

}

/**
 * Class WWRem2
 *
 * @package Parsers
 */

class WWRem2 extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "https://weworkremotely.com";

    protected $categories =     [
        "Main" => "https://weworkremotely.com/"

    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[2]/div/section[3]/article/ul/li[{{ITERATOR}}]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[2]/div/section[3]/article/ul/li[{{ITERATOR}}]/a/span[3]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[2]/div/section[3]/article/ul/li[{{ITERATOR}}]/a/span[2]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "order"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[2]/div[3]/section/div[1]",
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 2
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime(trim($value)));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderDescription($value){
        return strip_tags($value);
    }

    public function decorator_orderPrice(){
        return "0";
    }

}

/**
 * Class WWRem3
 *
 * @package Parsers
 */

class WWRem3 extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "https://weworkremotely.com";

    protected $categories =     [
        "Main" => "https://weworkremotely.com/"

    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[2]/div/section[4]/article/ul/li[{{ITERATOR}}]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[2]/div/section[4]/article/ul/li[{{ITERATOR}}]/a/span[3]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[2]/div/section[4]/article/ul/li[{{ITERATOR}}]/a/span[2]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "order"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[2]/div[3]/section/div[1]",
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 2
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime(trim($value)));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderDescription($value){
        return strip_tags($value);
    }

    public function decorator_orderPrice(){
        return "0";
    }

}

/**
 * Class WWRem4
 *
 * @package Parsers
 */

class WWRem4 extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "https://weworkremotely.com";

    protected $categories =     [
        "Main" => "https://weworkremotely.com/"

    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[2]/div/section[5]/article/ul/li[{{ITERATOR}}]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[2]/div/section[5]/article/ul/li[{{ITERATOR}}]/a/span[3]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[2]/div/section[5]/article/ul/li[{{ITERATOR}}]/a/span[2]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "order"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[2]/div[3]/section/div[1]",
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 2
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime(trim($value)));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderDescription($value){
        return strip_tags($value);
    }

    public function decorator_orderPrice(){
        return "0";
    }

}

/**
 * Class WWRem5
 *
 * @package Parsers
 */

class WWRem5 extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "https://weworkremotely.com";

    protected $categories =     [
        "Main" => "https://weworkremotely.com/"

    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[2]/div/section[6]/article/ul/li[{{ITERATOR}}]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[2]/div/section[6]/article/ul/li[{{ITERATOR}}]/a/span[3]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[2]/div/section[6]/article/ul/li[{{ITERATOR}}]/a/span[2]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "order"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[2]/div[3]/section/div[1]",
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 2
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime(trim($value)));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderDescription($value){
        return strip_tags($value);
    }

    public function decorator_orderPrice(){
        return "0";
    }

}

/**
 * Class WWRem6
 *
 * @package Parsers
 */

class WWRem6 extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "https://weworkremotely.com";

    protected $categories =     [
        "Main" => "https://weworkremotely.com/"

    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[2]/div/section[7]/article/ul/li[{{ITERATOR}}]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[2]/div/section[7]/article/ul/li[{{ITERATOR}}]/a/span[3]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[2]/div/section[7]/article/ul/li[{{ITERATOR}}]/a/span[2]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "order"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[2]/div[3]/section/div[1]",
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 2
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime(trim($value)));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderDescription($value){
        return strip_tags($value);
    }

    public function decorator_orderPrice(){
        return "0";
    }

}

/**
 * Class WWRem7
 *
 * @package Parsers
 */

class WWRem7 extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "https://weworkremotely.com";

    protected $categories =     [
        "Main" => "https://weworkremotely.com/"

    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[2]/div/section[8]/article/ul/li[{{ITERATOR}}]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[2]/div/section[8]/article/ul/li[{{ITERATOR}}]/a/span[3]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[2]/div/section[8]/article/ul/li[{{ITERATOR}}]/a/span[2]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "order"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[2]/div[3]/section/div[1]",
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 2
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime(trim($value)));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderDescription($value){
        return strip_tags($value);
    }

    public function decorator_orderPrice(){
        return "0";
    }

}


$wwr1 = new WWRem1();
$wwr1->startParsing();

$wwr2 = new WWRem2();
$wwr2->startParsing();

$wwr3 = new WWRem3();
$wwr3->startParsing();

$wwr4 = new WWRem4();
$wwr4->startParsing();

$wwr5 = new WWRem5();
$wwr5->startParsing();

$wwr6 = new WWRem6();
$wwr6->startParsing();

$wwr7 = new WWRem7();
$wwr7->startParsing();

