<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class Mediabistro
 *
 * @package Parsers
 */

class Mediabistro extends Parser{

    protected $ordersPerPage = 50;

    public $baseURL = "http://www.mediabistro.com";

    protected $categories =     [
        "Main" => "http://www.mediabistro.com/joblistings/default.asp?igid=0&zip_filter=0&jbrm=false&page=1&plen=50&trec=29&jbdr=0"
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "//*/table[@class='big-list dotted']/tbody/tr[{{ITERATOR}}]/td[1]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "//*/table[@class='big-list dotted']/tbody/tr[{{ITERATOR}}]/td[2]/span",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "//*/table[@class='big-list dotted']/tbody/tr[{{ITERATOR}}]/td[1]/a",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => '/html/body/div/div[2]/form/p[8]',
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "//*/table[@class='big-list dotted']/tbody/tr[{{ITERATOR}}]/td[4]/span",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 10,
        "orderIterator" => 1
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){

        if($value == "job of the day"){
            $date = "today";
        }else{
            $date = $value;
        }

        return date('Y-m-d H:i:s', strtotime($date));
    }

    public function decorator_orderType($value){
        return $value == "Full Time" ? $value : "fixed";
    }

    public function filter_orderType($value){
        return $value == "Full Time" ? false : true;
    }

    public function decorator_orderPrice($value){
        return "0";
    }

    public function decorator_orderTitle($value){
        return trim($value);
    }



}

$test = new Mediabistro();
$test->startParsing();