<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class jobsProBlogger
 *
 * @package Parsers
 */

class jobsProBlogger extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "http://jobs.problogger.net";

    protected $categories =     [
        "Main" => "http://jobs.problogger.net/jobs/{{ITERATOR}}"
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div/div/div[3]/div[1]/table/tr[{{ITERATOR}}]/td/span[2]/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div/div/div[3]/div[1]/table/tr[{{ITERATOR}}]/td/span[1]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div/div/div[3]/div[1]/table/tr[{{ITERATOR}}]/td/span[2]/a",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div/div/div[3]/div[1]/p[1]",
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => "true"
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        $date = explode("/", $value);
        return date('Y-m-d H:i:s', strtotime($date[1]."-".$date[0]."-".date("Y")));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice(){
        return "0";
    }



}

$test = new jobsProBlogger();
$test->startParsing();