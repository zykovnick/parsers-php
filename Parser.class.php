<?php

require_once('Parser.interface.php');
require_once('Order.class.php');

/**
 * Class Parser
 *
 * @package ParserCore
 */

abstract class Parser implements ParserInterface
{

    use Cli, OrderFilter;

    public $config = [];
    public $DBConnection;
    public $breakParsing;
    public $skills = [];

    public function __construct()
    {
        $this->Configuration();
        $this->DBConnect();
        $this->getSkills();

        //var_dump($this->config);
    }

    /**
     * Get list of skills and their aliases
     *
     * @return mixed
     */

    public function getSkills(){
        $skillsQuery = $this->DBConnection->query("SELECT `name`, `alias`  FROM skills");

        while($skill = $skillsQuery->fetch_array(MYSQL_ASSOC)){
            $checkArr = array($skill['name']);
            @$alias = array_merge( $checkArr, json_decode($skill['alias']));

            if(!$alias){
                $alias = $checkArr;
            }

            $alias = array_map('strtolower', $alias);

            $this->skills[] = array("tag_name" => $skill['name'], "aliases" => $alias);
        }
    }

    /**
     * Load config file
     */

    public function Configuration()
    {

        $config = parse_ini_file("config.ini");

        foreach ($config as $key => $value) {
            $this->config[$key] = $value;
        }

        if($this->configUniq){
            foreach($this->configUniq as $key => $value){
                $this->changeConfItem($key, $value);
            }
        }

    }

    /**
     * Database connection
     *
     * @todo Implement Singleton
     */

    public function DBConnect()
    {
        try {

            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            $this->DBConnection = new mysqli($this->config['dbHost'],
                $this->config['dbUser'],
                $this->config['dbPass'],
                $this->config['dbName']);

            $this->DBConnection->set_charset("utf8");

        } catch (Exception $e) {
            echo "Service unavailable" . PHP_EOL;
            echo "Message: " . $e->getMessage();
            exit;
        }
    }

    /**
     * Creating new object Order
     *
     * @param   object $categoryDOM DOM object of parent category page where link to order is placed
     * @param   string $orderIterator Counting number of order on category page
     * @param   Parser $parser Self object of current parser
     * @return  object  Order           Order object
     */

    public function Order($categoryDOM, $orderIterator, Parser $parser)
    {
        return new Order($categoryDOM, $orderIterator, $parser);
    }

    /**
     * Changing configuration parameter. Can be used for creating new configuration option
     *
     * @param   string $key Key of parameter in configuration file
     * @param   string $value Value on which will be changed key
     */

    public function changeConfItem($key, $value)
    {
        $this->config[$key] = $value;
    }

    /**
     * Grabbing page source by URL
     *
     * @param   string $pageURL Page of URL that will be parsed
     * @return  mixed               HTML of page
     */

    private function getPageHTML($pageURL)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_REFERER, $this->config['defaultReferrer']);
        curl_setopt($ch, CURLOPT_USERAGENT, "Opera/9.80 (Windows NT 5.1; U; eng) Presto/2.9.168 Version/11.51");
        curl_setopt($ch, CURLOPT_URL, $pageURL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $html = curl_exec($ch);

        curl_close($ch);

        return $html;
    }

    /**
     * Converting HTML to DOMXPath object
     *
     * @param   string $pageURL Page of URL that will be parsed
     * @return  DOMXPath
     */

    public function getPageDOMxPath($pageURL)
    {

        $html = $this->getPageHTML($pageURL);

        //file_put_contents('test.html', $html, FILE_APPEND);

        $DOM = new DOMDocument;
        libxml_use_internal_errors(true);

        for ($try = 1; $try <= 5; $try++) {
            if (!$DOM->loadHTML($html)) {
                echo "Page loading is fault!".PHP_EOL;

                $html = $this->getPageHTML($pageURL);

                $DOM = new DOMDocument;
                libxml_use_internal_errors(true);

                if($try == 5){
                    $errors = "";
                    foreach (libxml_get_errors() as $error) {
                        $errors .= $error->message . PHP_EOL;
                    }
                    libxml_clear_errors();
                    print "Libxml errors:<br>$errors" . PHP_EOL;
                }
            } else {
                break;
            }
        }

        $xpath = new DOMXPath($DOM);

        return $xpath;
    }

    /**
     * Changing logic of counting order iterator
     *
     * @param $step
     * @return mixed
     */

    public function iteratorValue($step){
        return $step;
    }


    /**
     * Starting process of parsing site
     */

    public function startParsing()
    {

        foreach ($this->categories as $categoryName => $categoryURL) {

            $this->breakParsing = false;

            $this->showMessage($categoryName . " is going to be parsed", "info");
            $pageIterator = 1;

            while ($pageIterator <= $this->config['maxPage']) {

                $this->showMessage("Page number ".$pageIterator, "text");

                $curCategoryURL = str_replace("{{ITERATOR}}", $pageIterator, $categoryURL);

                $curCategoryDOM = $this->getPageDOMxPath($curCategoryURL);

                $orderIterator = $this->config['orderIterator'];

                while ($orderIterator <= $this->ordersPerPage) {

                    if(!$this->breakParsing){

                        $order = $this->Order($curCategoryDOM, $this->iteratorValue($orderIterator), $this);
                        $order->processOrder();
                    }else{
                        break 2;
                    }

                    $orderIterator++;
                }

                $pageIterator++;
            }

        }

    }


}



