<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 18.07.15
 * Time: 21:11
 */

/**
 * Interface ParserInterface
 *
 * @package ParserCore
 */

interface ParserInterface{

    public function getSkills();
    public function Configuration();
    public function DBConnect();
    public function startParsing();

}