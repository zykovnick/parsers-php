<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');


/**
 * Class Flexijobs
 *
 * @package Parsers
 */

class Flexijobs extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "http://www.flexjobs.com/";

    protected $categories =     [
        "Main" => "http://www.flexjobs.com/jobs/web-design?page={{ITERATOR}}"

    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[2]/div/div[3]/div[2]/ul/li[{{ITERATOR}}]/div/div[1]/h5/a/@href",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[2]/div/div[3]/div[2]/div[2]/div/table/tr[1]/td",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[2]/div/div[3]/h1",
            "count" => "single",
            "page"  => "order"
        ],
        "orderSkills" =>    [
            "xPath" => "/html/body/div[2]/div/div[3]/div[2]/div[2]/div/table/tr[2]/td/a",
            "count" => "multiple",
            "page"  => "order"
        ],
        "orderDescription" => [
            "xPath" => "/html/body/div[2]/div/div[3]/div[1]/div[2]/div/p",
            "count" => "single",
            "page"  => "order"
        ],
        "orderType" =>      [
            "xPath" => "/html/body/div[2]/div/div[3]/div[2]/ul/li[{{ITERATOR}}]/div/div[1]/p/span[1]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderPrice" =>     [
            "xPath" => "/test",
            "count" => "single",
            "page"  => "order",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 2
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        $time = explode("/", trim($value));
        return date('Y-m-d H:i:s', strtotime($time[2]."-".$time[0]."-".$time[1]));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice($value){
        return "0";
    }

    public function decorator_orderSkills($value){
        return $value;
    }


    public function filter_orderType($value){
       if (strpos(strtolower($value),'freelance') !== false) {
           Order::$orderValues["orderType"] = "fixed";
         return true;
       }else{
         return false;
       }
    }



}

$test = new Flexijobs();
$test->startParsing();