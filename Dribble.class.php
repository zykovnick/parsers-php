<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class Dribble
 *
 * @package Parsers
 */

class Dribble extends Parser{

    protected $ordersPerPage = 10;

    public $baseURL = "";

    protected $categories =     [
        "Main" => "https://dribbble.com/jobs?location=Anywhere"
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => "/html/body/div[3]/div[2]/div/div[2]/div/ol[1]/li[{{ITERATOR}}]/a[@class='item-link group']/@data-url",
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "/html/body/div[3]/div[2]/div/div[2]/div/ol[1]/li[{{ITERATOR}}]/a[@class='item-link group']/@title",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "/html/body/div[3]/div[2]/div/div[2]/div/ol[1]/li[{{ITERATOR}}]/a[@class='item-link group']/span[1]/span[2]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "/null",
            "count" => "multiple",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => "true"
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 1
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){

        $date = trim(substr($value, 6));

        return date('Y-m-d H:i:s', strtotime($date));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice(){
        return "0";
    }

    public function decorator_orderDescription(){
        return "Unfortunately, there is no description for this order.";
    }


}

$test = new Dribble();
$test->startParsing();