<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 15.07.15
 * Time: 16:05
 */

/**
 * Class SkillsDetector
 *
 * @package ParserCore
 */

trait SkillsDetector{

    /**
     * Find suitable tags for order
     *
     * @param $title
     * @param $description
     * @param null $skills
     * @return array
     */

    public function processSkills($title, $description, $skills = null){


        $submitSkills = [];

        if($skills){
            $skills = json_decode($skills);

            foreach($skills as $skill){
                foreach($this->parser->skills as $parserSkill){
                        foreach($parserSkill['aliases'] as $alias){
                            if(similar_text(strtolower(trim(strip_tags($skill))), $alias) > 50){
                                if(!in_array($parserSkill['tag_name'], $submitSkills)){
                                    $submitSkills[] = $parserSkill['tag_name'];
                                }
                            }
                        }

                }
            }
        }

        if($description){
            foreach($this->parser->skills as $parserSkill){
                    foreach($parserSkill['aliases'] as $alias){
                        if( (strpos(strtolower($description), $alias) !== false) OR
                            (strpos(strtolower($title), strtolower($alias)) !== false)){
                            if(!in_array($parserSkill['tag_name'], $submitSkills)){
                                $submitSkills   [] = $parserSkill['tag_name'];
                            }
                        }
                    }
            }
        }

        return $submitSkills;

        /*
        $skills = [];
        $submitSkills = [];

        $concepts = $this->alchemy->concepts('text',$text, null);
        $entities = $this->alchemy->entities('text',$text, null);
        $keywords = $this->alchemy->keywords('text',$text, null);

        foreach($concepts['concepts'] as $concept){
            if($concept['relevance'] >= 0.4 AND ($concept['sentiment']['type'] != "negative" )){
                    $skills[] = strtolower($concept["text"]);
            }
        }


        foreach($keywords['keywords'] as $keyword){
            if( ($keyword['relevance'] >= 0.4) AND ($keyword['sentiment']['type'] != "negative" )){
                $skills[] = strtolower($keyword["text"]);
            }
        }

        foreach($entities['entities'] as $entity){
            if($entity['relevance'] >= 0.4 AND ($entity['sentiment']['type'] != "negative" ) ){
                $skills[] = strtolower($entity["text"]);
            }
        }

        foreach($this->parser->skills as $parserSkill){
            foreach($skills as $skill){
                foreach($parserSkill['aliases'] as $alias){
                    similar_text($alias, $skill, $sim);

                    if($sim > 90){
                        if(!in_array($parserSkill['tag_name'], $submitSkills)){
                            $submitSkills[] = $parserSkill['tag_name'];
                        }
                    }
                }
            }
        }

        return $submitSkills;

        */

    }

}