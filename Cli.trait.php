<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12.07.15
 * Time: 22:48
 */

/**
 * Class Cli
 *
 * @package ParserCore
 */
    
trait Cli{

    private $foregroundColors = array(
        "black" => "0;30",
        "dark_gray" => "1;30",
        "blue" => "0;34",
        "light_blue" => "1;32",
        "cyan" => "0;36",
        "light_cyan" => "1;36",
        "red" => "0;31",
        "light_red" => "1;31",
        "purple" => "0;35",
        "light_purple" => "1;35",
        "brown" => "0;33",
        "yellow" => "1;33",
        "light_grey" => "0;37",
        "white" => "1;37"
    );
    private $backgroundCcolors = array(
        "black" => "40",
        "red" => "41",
        "green" => "42",
        "yellow" => "43",
        "blue" => "44",
        "magenta" => "45",
        "cyan" => "46",
        "light_grey" => "47"
    );


    /**
     * @param $string
     * @param null $foregroundColor
     * @param null $backgroundColor
     * @return string
     */

    public function getColoredString($string, $foregroundColor = null, $backgroundColor = null) {
        $colored_string =  chr(27)."[".$this->foregroundColors[$foregroundColor].";".$this->backgroundColors[$backgroundColor]."m".$string.chr(27)."[0m";
        return $colored_string;
    }

    /**
     * @param $text
     * @param $type
     */

    public function showMessage($text, $type){
        switch ($type){
            case "error":
                echo $this->getColoredString(PHP_EOL."ERROR: ".$text.PHP_EOL, "red", "white");
                break;

            case "info":
                echo $this->getColoredString(PHP_EOL.$text.PHP_EOL, "light_blue", "white");
                break;

            case "text":
                echo $this->getColoredString(PHP_EOL.$text.PHP_EOL, "yellow", "light_grey");
                break;

            default;
        }
    }

}