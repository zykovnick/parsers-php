<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 13.07.15
 * Time: 2:58
 */

require_once('langDetect/Text/LanguageDetect.php');


/**
 * Class OrderFilter
 *
 * @package ParserCore
 */

trait OrderFilter{

    /**
     * @param string $description Text of order description
     * @return boolean  Is language of description is valid
     */

    private function orderLanguage($description)
    {
        $lang = new Text_LanguageDetect;
        $lang_result = $lang->detectConfidence($description);

        return $lang_result['language'] ==  $this->config["orderLanguage"] ? true : false;
    }

    /**
     * @param string $description Text of order description
     * @return boolean  Is description has valid length
     */

    private function orderLength($description)
    {
        return strlen($description) >= $this->config['minChars'] ?  true : false;
    }

    /**
     * @param string $value Text of order description
     * @return boolean Is description valid. Passing through @see orderLanguage , @see orderLength
     */

    public function filter_orderDescription($value)
    {
       return (
                $this->orderLanguage($value) AND
                $this->orderLength($value)
              ) ? true : false;
    }

    public function filter_postedTime($value)
    {
        if( time() - strtotime($value) > $this->config["orderMaxAge"] ){
            $this->breakParsing = true;
            return false;
        }else{
            return true;
        }
    }

}
