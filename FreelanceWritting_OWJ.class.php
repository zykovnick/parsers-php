<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 22.07.15
 * Time: 23:48
 */

require_once('Cli.trait.php');
require_once('Parser.class.php');

/**
 * Class FreelanceWritting_OWJ
 *
 * @package Parsers
 */

class FreelanceWritting_OWJ extends Parser{

    protected $ordersPerPage = 20;

    public $baseURL = "";

    protected $categories = [
        "Online writing jobs" => "http://www.freelancewriting.com/freelancejobs/onlinewritingjobs.php",
    ];

    public $parsePathes = [
        "orderURL" =>       [
            "xPath" => '//*/div[{{ITERATOR}}]/div/div/h2/a/@href',
            "count" => "single",
            "page"  => "category"
        ],
        "postedTime" =>     [
            "xPath" => "//*/div[{{ITERATOR}}]/div/div/p[6]",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderTitle" =>     [
            "xPath" => "//*/div[{{ITERATOR}}]/div/div/h2/a",
            "count" => "single",
            "page"  => "category"
        ],
        "orderSkills" =>    [
            "xPath" => "//*/div[{{ITERATOR}}]/div/div/p[4]/span",
            "count" => "multiple",
            "page"  => "category"
        ],
        "orderDescription" => [
            "xPath" => "//*/div[{{ITERATOR}}]/div/div/p[1]",
            "count" => "single",
            "page"  => "category"
        ],
        "orderType" =>      [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ],
        "orderPrice" =>     [
            "xPath" => "/null",
            "count" => "single",
            "page"  => "category",
            "decorator" => true
        ]
    ];


    public $configUniq = [
        "orderMaxAge" => 151200,
        "minChars" => 50,
        "orderIterator" => 1
    ];


    public function __construct(){
        parent::__construct();
    }

    public function decorator_postedTime($value){
        return date('Y-m-d H:i:s', strtotime($value));
    }

    public function decorator_orderType(){
        return "fixed";
    }

    public function decorator_orderPrice(){
        return "0";
    }


}

$test = new FreelanceWritting_OWJ();
$test->startParsing();